(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(auth-source-save-behavior nil)
 '(custom-safe-themes
   '("e3b2bad7b781a968692759ad12cb6552bc39d7057762eefaf168dbe604ce3a4b" default))
 '(exwm-floating-border-color "#191b20")
 '(fci-rule-color "#5B6268")
 '(highlight-tail-colors
   ((("#333a38" "#99bb66" "green")
     . 0)
    (("#2b3d48" "#46D9FF" "brightcyan")
     . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#51afef"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#98be65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(ob-ein-languages
   '(("ein" . python)
     ("ein-python" . python)
     ("ein-R" . R)
     ("ein-r" . R)
     ("ein-julia" . julia)
     ("ein-maple" . maple)))
 '(objed-cursor-color "#ff6c6b")
 '(org-agenda-files '("/home/nonuser/Documents/math300/homework-1.org"))
 '(pdf-view-midnight-colors (cons "#bbc2cf" "#282c34"))
 '(rustic-ansi-faces
   ["#282c34" "#ff6c6b" "#98be65" "#ECBE7B" "#51afef" "#c678dd" "#46D9FF" "#bbc2cf"])
 '(vc-annotate-color-map
   (list
    (cons 20 "#98be65")
    (cons 40 "#b4be6c")
    (cons 60 "#d0be73")
    (cons 80 "#ECBE7B")
    (cons 100 "#e6ab6a")
    (cons 120 "#e09859")
    (cons 140 "#da8548")
    (cons 160 "#d38079")
    (cons 180 "#cc7cab")
    (cons 200 "#c678dd")
    (cons 220 "#d974b7")
    (cons 240 "#ec7091")
    (cons 260 "#ff6c6b")
    (cons 280 "#cf6162")
    (cons 300 "#9f585a")
    (cons 320 "#6f4e52")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil)
 '(warning-suppress-log-types '((initialization) (initialization)))
 '(warning-suppress-types
   '((org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     (initialization))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

                                        ; (setenv "XDG_CURRENT_DESKTOP" "gnome")
                                        ; (setenv "XDG_CURRENT_DESKTOP" "gnome")
                                        ; (setenv "DESKTOP_SESSION" "gnome")
                                        ; (setenv "KDE_FULL_SESSION" "")

(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "~/.elan/bin")))
(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "~/.cargo/bin")))

(add-to-list 'exec-path "/home/nonuser/.elan/bin")
(add-to-list 'exec-path "/home/nonuser/.cargo/bin")
