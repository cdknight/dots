;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Ramesh Balaji"
      user-mail-address "post2rb@hotmail.com")

(setq doom-font (font-spec :family "Jetbrains Mono" :size 15)
      doom-variable-pitch-font (font-spec :family "Noto Serif" :size 16))

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(after! rustic
  (setq rustic-flycheck-clippy-params "--message-format=json")
  )

                                        ;(setq elcord-use-major-mode-as-main-icon t)
                                        ;(elcord-mode)

(setq org-crypt-key "me@ramesh.tv")

(setq doom-themes-treemacs-enable-variable-pitch nil)
;; (use-package! adoc-mode
;;   :hook (adoc-mode . mixed-pitch-mode)
;;   :config
;;   (dolist (i (number-sequence 0 5))
;;      (set-face-attribute
;;       (intern (format "markup-title-%d-face" i)) nil :height 1.4))
;;    :mode "\\.adoc$")

;; (setq writeroom-local-effects '(mixed-pitch-mode))

(with-eval-after-load 'ox-latex
  (progn
    (add-to-list 'org-latex-packages-alist
                 '("margin=2cm" "geometry" nil))
    (add-to-list 'org-latex-classes
                 '("story"
                   "\\documentclass[12pt]{article}
                        [DEFAULT-PACKAGES]
                        [PACKAGES]
                        \\usepackage{fontspec}
                        \\setmainfont{Liberation Serif}"
                   ("\\section{%s}" . "\\section*{%s}")
                   ("\\subsection{%s}" . "\\subsection*{%s}")
                   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                   ("\\paragraph{%s}" . "\\paragraph*{%s}")
                   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
    ))

(with-eval-after-load 'julia-repl
  (julia-repl-set-terminal-backend 'vterm))

(add-hook! 'writeroom-mode-hook
  (display-line-numbers-mode (if writeroom-mode -1 +1)))

(with-eval-after-load 'org
  org-format-latex-options (plist-put org-format-latex-options :scale 3.0)
  'turn-on-org-cdlatex)

(with-eval-after-load 'c
  (setq c-basic-offset 4))

(setq org-startup-with-inline-images t
      org-startup-with-latex-preview t
      org-html-checkbox-type 'html
      org-highlight-latex-and-related '(latex script entities)
      org-latex-pdf-process '("xelatex -interaction nonstopmode %f"))

(setq fancy-splash-image (concat doom-private-dir "Water2.png"))

(use-package! org
  :config
  (map! :leader
        :desc "Copy cell to clipboard"
        "m b y" (lambda() (interactive)
                  (when (org-at-table-p)
                    (kill-new
                     (string-trim
                      (substring-no-properties(org-table-get-field)))))
                  org-mode-map))
  )


(setq-default tab-width 4)
(setq c-set-style "k&r")
(setq c-basic-offset 4)
(setq TeX-command-extra-options " -shell-escape ")

(add-to-list 'auto-mode-alist '("\\.lean\\'" . lean4-mode))
(add-to-list 'auto-mode-alist '("\\.prolog\\'" . prolog-mode))
(add-to-list 'auto-mode-alist '("\\.yap\\'" . prolog-mode))

(add-to-list 'load-path (concat "/usr/share/emacs/site-lisp/maple"))
(autoload 'maplev-mode "maplev" "Maple editing mode" 'interactive)
(add-to-list 'auto-mode-alist '("\\.mpl\\'" . maplev-mode))

;; If auto formating is annoying :
;; To enable it, just eval it M-:
(add-hook! 'before-save-hook #'+format/buffer)
(remove-hook! 'before-save-hook #'+format/buffer)



;; fix treemacs

(defun toggle-treemacs-good ()
  "Initialize or toggle treemacs.

Ensures that only the current project is present and all other projects have
been removed.

Use `treemacs' command for old functionality."
  (interactive)
  (cond (  (equal (treemacs-current-visibility) nil) (delete-window (treemacs-get-local-window))  )
        ( t (treemacs) ) )

  )

(map!       :leader "o p" #'toggle-treemacs-good)
