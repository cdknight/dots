set -xg GOPATH $HOME/go
set -xg PATH $GOPATH/bin $HOME/.cargo/bin $HOME/.local/bin $HOME/.gem/ruby/2.7.0/bin $HOME/gcc-linaro-arm/bin $HOME/flutter/flutter/bin $PATH
set -xg EDITOR nvim
set -xg SSH_ASKPASS /usr/bin/ksshaskpass
set -xg SSH_AUTH_SOCK $XDG_RUNTIME_DIR/ssh-agent.socket

fenv source /etc/profile.d/nix{,-daemon}.sh 

