#!/usr/bin/env python3

# Largely derived from https://github.com/altdesktop/playerctl/blob/master/examples

import gi
gi.require_version("Playerctl", "2.0")

from gi.repository import Playerctl, GLib

mgr = Playerctl.PlayerManager()

def on_play(player, status, manager):
    update_playing(player)

def on_meta(player, metadata, e):
    update_playing(player)

def update_playing(player, only_playing=False):
    # Only playing is for startup where we want to show the track that's playing instead of the
    # paused track
    status = player.props.playback_status
    if only_playing and status == Playerctl.PlaybackStatus.PAUSED:
        return

    artist = dict(player.props.metadata).get("xesam:artist")
    if artist and artist != ['']:
        artist = f" {artist[0]} - " 
    else:
        artist = " "

    icon = "\uf04b" if status == Playerctl.PlaybackStatus.PAUSED else "\uf04c"
    try:
        print(f"{icon}{artist}{player.props.metadata['xesam:title']}")
    except KeyError:
        # ignore this
        pass

def on_name_appeared(manager, name):
    init_player(name)

def init_player(name):
    # choose if you want to manage the player based on the name
    player = Playerctl.Player.new_from_name(name)
    player.connect('metadata', on_meta, mgr)
    player.connect('playback-status::playing', on_play, mgr)
    player.connect('playback-status::paused', on_play, mgr)
    mgr.manage_player(player)
    update_playing(player, True)

mgr.connect('name-appeared', on_name_appeared)

# Add the currently existing players
for name in mgr.props.player_names:
    init_player(name)

main = GLib.MainLoop()
main.run()

