#!/usr/bin/env bash

unset GDK_SCALE
unset GDK_DPI_SCALE

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch bar1 and bar2

PRIMARY=$(xrandr --query | grep " connected" | grep "primary" | cut -d" " -f1)
OTHERS=$(xrandr --query | grep " connected" | grep -v "primary" | cut -d" " -f1)

MONITOR=$PRIMARY polybar --reload mybar &
sleep 1

for m in $OTHERS; do
    MONITOR=$m polybar --reload mybar & 
done

echo "Bar launched..."
